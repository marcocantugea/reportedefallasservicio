require 'test_helper'

class FailingServicesControllerTest < ActionController::TestCase
  setup do
    @failing_service = failing_services(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:failing_services)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create failing_service" do
    assert_difference('FailingService.count') do
      post :create, failing_service: { AffectedService: @failing_service.AffectedService, DateTicket: @failing_service.DateTicket, GeneratedBy: @failing_service.GeneratedBy, Priotiry: @failing_service.Priotiry, ProblemDescriprion: @failing_service.ProblemDescriprion, ProblemResolution: @failing_service.ProblemResolution, TicketHistory: @failing_service.TicketHistory, TicketNum: @failing_service.TicketNum, TicketTitle: @failing_service.TicketTitle, TimeOfSolution: @failing_service.TimeOfSolution }
    end

    assert_redirected_to failing_service_path(assigns(:failing_service))
  end

  test "should show failing_service" do
    get :show, id: @failing_service
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @failing_service
    assert_response :success
  end

  test "should update failing_service" do
    patch :update, id: @failing_service, failing_service: { AffectedService: @failing_service.AffectedService, DateTicket: @failing_service.DateTicket, GeneratedBy: @failing_service.GeneratedBy, Priotiry: @failing_service.Priotiry, ProblemDescriprion: @failing_service.ProblemDescriprion, ProblemResolution: @failing_service.ProblemResolution, TicketHistory: @failing_service.TicketHistory, TicketNum: @failing_service.TicketNum, TicketTitle: @failing_service.TicketTitle, TimeOfSolution: @failing_service.TimeOfSolution }
    assert_redirected_to failing_service_path(assigns(:failing_service))
  end

  test "should destroy failing_service" do
    assert_difference('FailingService.count', -1) do
      delete :destroy, id: @failing_service
    end

    assert_redirected_to failing_services_path
  end
end
