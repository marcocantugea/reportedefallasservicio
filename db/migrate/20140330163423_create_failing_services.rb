class CreateFailingServices < ActiveRecord::Migration
  def change
    create_table :failing_services do |t|
      t.integer :TicketNum
      t.string :TicketTitle
      t.string :AffectedService
      t.string :Priotiry
      t.string :GeneratedBy
      t.datetime :DateTicket
      t.text :ProblemDescriprion
      t.text :TicketHistory
      t.text :TimeOfSolution
      t.text :ProblemResolution

      t.timestamps
    end
  end
end
