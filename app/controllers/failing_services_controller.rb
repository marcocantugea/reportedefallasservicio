class FailingServicesController < ApplicationController
  before_action :set_failing_service, only: [:show, :edit, :update, :destroy]

  # GET /failing_services
  # GET /failing_services.json
  def index
    @failing_services = FailingService.all
  end

  # GET /failing_services/1
  # GET /failing_services/1.json
  def show
  end

  # GET /failing_services/new
  def new
    @failing_service = FailingService.new
  end

  # GET /failing_services/1/edit
  def edit
  end

  # POST /failing_services
  # POST /failing_services.json
  def create
    @failing_service = FailingService.new(failing_service_params)

    respond_to do |format|
      if @failing_service.save
        format.html { redirect_to @failing_service, notice: 'Failing service was successfully created.' }
        format.json { render action: 'show', status: :created, location: @failing_service }
      else
        format.html { render action: 'new' }
        format.json { render json: @failing_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /failing_services/1
  # PATCH/PUT /failing_services/1.json
  def update
    respond_to do |format|
      if @failing_service.update(failing_service_params)
        format.html { redirect_to @failing_service, notice: 'Failing service was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @failing_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /failing_services/1
  # DELETE /failing_services/1.json
  def destroy
    @failing_service.destroy
    respond_to do |format|
      format.html { redirect_to failing_services_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_failing_service
      @failing_service = FailingService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def failing_service_params
      params.require(:failing_service).permit(:TicketNum, :TicketTitle, :AffectedService, :Priotiry, :GeneratedBy, :DateTicket, :ProblemDescriprion, :TicketHistory, :TimeOfSolution, :ProblemResolution)
    end
end
