json.array!(@failing_services) do |failing_service|
  json.extract! failing_service, :id, :TicketNum, :TicketTitle, :AffectedService, :Priotiry, :GeneratedBy, :DateTicket, :ProblemDescriprion, :TicketHistory, :TimeOfSolution, :ProblemResolution
  json.url failing_service_url(failing_service, format: :json)
end
